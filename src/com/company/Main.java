package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

public class Main {

    public static void main(String[] args) throws IOException {
	// write your code here
        /*ModelMVC add = new ModelMVC();
        ViewMVC print = new ViewMVC();
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int num1 = Integer.parseInt(reader.readLine());
        int num2 = Integer.parseInt(reader.readLine());
        print.print(add.add(num1, num2));

        System.out.println();

        RemoveDuplicates remover = new RemoveDuplicates();
        Generics printerAnything = new Generics();
        int array[] = new int[]{5, 5, 5, 6, 6, 5 , 53, 5, 3, 7, 7};
        HashSet nonRepeatedArray = remover.removeDuplicate(array);
        printerAnything.printAnything(nonRepeatedArray);*/
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        List<Employee> employees = new ArrayList<Employee>();
        employees.add(new Employee(1, "Ram", 22));
        employees.add(new Employee(2, "Shyam", 52));
        employees.add(new Employee(3, "Rahul", 26));
        employees.add(new Employee(4, "Sameer", 22));
        employees.add(new Employee(5, "Raja", 27));

        Comparator<Employee> compareById = (Employee o1, Employee o2) -> (Integer.compare(o1.getId(),o2.getId()));
        Comparator<Employee> compareByAge = (Employee o1, Employee o2) -> (Integer.compare(o1.getAge(),o2.getAge()));
        Comparator<Employee> compareByName = (Employee o1, Employee o2) -> (CharSequence.compare(o1.getName(),o2.getName()));

        System.out.println("Compare by \n1.Age\n2.Id\n3.Name");
        int choice = Integer.parseInt(reader.readLine());
        switch (choice)
        {
            case 1:
            {Collections.sort(employees, compareByAge); break;}
            case 2:
            {Collections.sort(employees, compareById); break;}
            case 3:
            {Collections.sort(employees, compareByName); break;}
        }
        System.out.println(employees);
    }
}
