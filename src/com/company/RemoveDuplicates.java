package com.company;

import java.util.HashMap;
import java.util.HashSet;

public class RemoveDuplicates {
    public HashSet<Integer> removeDuplicate(int[] array){
        HashSet<Integer> set = new HashSet<Integer>();
        for(int num : array)
        {
            set.add(num);
        }
        return set;
    }
}
